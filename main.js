wrapText = function(word){
  var text = word.selectionText;
	copyToClipboard(text);
};


function copyToClipboard( text ){
                var copyDiv = document.createElement('div');
                copyDiv.contentEditable = true;
                document.body.appendChild(copyDiv);
                copyDiv.innerHTML = text;
                copyDiv.unselectable = "off";
                copyDiv.focus();
                document.execCommand('SelectAll');
                document.execCommand("Copy", false, null);
                document.body.removeChild(copyDiv);
            }

chrome.contextMenus.create({
  title: "Wrap Text to time sheet",
  contexts:["selection"],
  onclick: wrapText
});